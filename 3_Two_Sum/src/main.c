
/*
 *
 * Create a function that takes a Roman numeral
 * as its argument and returns its value as a
 * numeric decimal integer. You don't need to validate
 * the form of the Roman numeral.
 *
 * Modern Roman numerals are written by expressing each
 * decimal digit of the number to be encoded separately,
 * starting with the leftmost digit and skipping any 0s.
 * So 1990 is rendered "MCMXC"
 * (1000 = M, 900 = CM, 90 = XC) and 2008 is rendered
 * "MMVIII" (2000 = MM, 8 = VIII). The Roman numeral
 * for 1666, "MDCLXVI", uses each letter in descending
 * order.
 *
 */

#include <stdio.h>
#include <string.h>

void twoSum (unsigned count, const int numbers[count], int target,
             unsigned *index1, unsigned *index2);

int
main (void)
{
  unsigned count = 3;
  const int numbers[3] = { 1, 2, 3 };
  int target = 4;
  unsigned index1 = 0;
  unsigned index2 = 0;

  twoSum(count, numbers, target, &index1, &index2);
  printf ("Data: 1 2 3 Result: %d %d Expected: %d %d\n", index1, index2, 0, 2);

  return 0;
}

void
twoSum (unsigned count, const int numbers[count], int target, unsigned *index1,
        unsigned *index2)
{
  for (int i = 0; i < (int)count; i++)
    for (int j = 0; j < (int)count; j++)
      if (numbers[i] + numbers[j] == target)
        {
          *index1 = i;
          *index2 = j;
        }
}
