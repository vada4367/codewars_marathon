
/*
 *
 * Create a function that takes a Roman numeral
 * as its argument and returns its value as a
 * numeric decimal integer. You don't need to validate
 * the form of the Roman numeral.
 *
 * Modern Roman numerals are written by expressing each
 * decimal digit of the number to be encoded separately,
 * starting with the leftmost digit and skipping any 0s.
 * So 1990 is rendered "MCMXC"
 * (1000 = M, 900 = CM, 90 = XC) and 2008 is rendered
 * "MMVIII" (2000 = MM, 8 = VIII). The Roman numeral
 * for 1666, "MDCLXVI", uses each letter in descending
 * order.
 *
 */

#include <stdio.h>
#include <string.h>

unsigned decode_roman (const char *roman_number);

int
main (void)
{
  const char *roman_number = "MDCLXVI";
  printf ("Data: %s Result: %d Expected: %d\n", roman_number,
          decode_roman (roman_number), 1666);

  return 0;
}

int
find_num (char *syms, int *values, char sym)
{
  for (int i = 0; i < (int)strlen (syms); i++)
    if (syms[i] == sym)
      return values[i];

  return -1;
}

unsigned
decode_roman (const char *roman_number)
{
  char syms[8] = "IVXLCDM";
  int values[8] = { 1, 5, 10, 50, 100, 500, 1000 };
  int now_num, next_num;
  unsigned result = 0;

  while (*roman_number)
    {
      now_num = find_num (syms, values, *roman_number);
      next_num = find_num (syms, values, *(roman_number + 1));

      if (now_num >= next_num || next_num == -1)
        result += now_num;
      else
        result -= now_num;

      roman_number++;
    }

  return result;
}
