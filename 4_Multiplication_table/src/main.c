
/*
 *
 * Your task, is to create N×N multiplication table, of size provided in
 * parameter. For example, when given size is 3:
 *
 * 1 2 3
 * 2 4 6
 * 3 6 9
 *
 * For the given example, the return value should be:
 * [[1,2,3],[2,4,6],[3,6,9]]
 *
 * Note: in C, you must return an allocated table of allocated rows
 *
 */

#include <stdio.h>
#include <stdlib.h>

int **multiplication_table (int n);
void print_array (int **array);

int
main (void)
{
  int n = 3;
  print_array (multiplication_table (n));
  printf (
      "Data: %d Result: Fucking c language Expected: fucking c languauge\n",
      n);

  return 0;
}

int **
multiplication_table (int n)
{
  int **result = (int **)malloc (n * sizeof (int *));
  for (int i = 0; i < n; i++)
    result[i] = (int *)malloc (n * sizeof (int));

  for (int i = 0; i < n; i++)
    for (int j = 0; j < n; j++)
      result[i][j] = (i + 1) * (j + 1);

  return result;
}

void
print_array (int **array)
{
  for (int i = 0; i < 3; i++)
    {
      for (int j = 0; j < 3; j++)
        {
          printf ("%d ", array[i][j]);
        }
      printf ("\n");
    }
}
