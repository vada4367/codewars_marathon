
/*
 * Write a function that takes an integer 
 * as input, and returns the number of bits 
 * that are equal to one in the binary representation 
 * of that number. You can guarantee that input 
 * is non-negative.
 * Example: The binary representation of 1234 is 10011010010, 
 * so the function should return 5 in this case
 */

#include <stddef.h>
#include <stdio.h>
#include <string.h>

size_t countBits (unsigned value);

int
main (void)
{
  unsigned value = 1234;
  printf ("Data: %d Result: %ld Expected: %d\n", value, countBits (value), 5);

  return 0;
}

size_t
countBits (unsigned value)
{
  size_t result = 0;

  for (int i = 0; i < 32; i++)
    {
      result += 1 & value;
      value = value >> 1;
    }

  return result;
}

